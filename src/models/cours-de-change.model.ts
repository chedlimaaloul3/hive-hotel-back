import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Bureau} from './bureau.model';

@model()
export class CoursDeChange extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom: string;
  @property({
    type: 'string',
  })
  currency?: string;
  @property({
    type: 'string',
    required: true,
  })
  logo: string;

  @property({
    type: 'number',
    required: true,
  })
  unite: number;

  @property({
    type: 'number',
    required: true,
  })
  achat: number;

  @belongsTo(() => Bureau)
  bureauId: string;

  constructor(data?: Partial<CoursDeChange>) {
    super(data);
  }
}

export interface CoursDeChangeRelations {
  // describe navigational properties here
}

export type CoursDeChangeWithRelations = CoursDeChange & CoursDeChangeRelations;
