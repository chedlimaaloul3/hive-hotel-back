import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {User} from './user.model';
import {Devises} from './devises.model';

@model()
export class Caisse extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    type: 'string',
    required: true,
  })
  etat: string;

  @property({
    type: 'string',
  })
  date?: string;

  @belongsTo(() => User)
  userId: string;

  @hasMany(() => Devises)
  devises: Devises[];

  constructor(data?: Partial<Caisse>) {
    super(data);
  }
}

export interface CaisseRelations {
  // describe navigational properties here
}

export type CaisseWithRelations = Caisse & CaisseRelations;
