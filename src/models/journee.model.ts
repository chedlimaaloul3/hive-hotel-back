import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Bureau} from './bureau.model';

@model()
export class Journee extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  etat: string;

  @property({
    type: 'string',
  })
  date_ouverture?: string;

  @property({
    type: 'string',
  })
  date_fermuture?: string;

  @property({
    type: 'string',
  })
  code_user_ouv?: string;

  @property({
    type: 'string',
  })
  code_user_ferm?: string;

  @belongsTo(() => Bureau)
  bureauId: string;

  constructor(data?: Partial<Journee>) {
    super(data);
  }
}

export interface JourneeRelations {
  // describe navigational properties here
}

export type JourneeWithRelations = Journee & JourneeRelations;
