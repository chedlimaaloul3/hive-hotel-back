import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Caisse} from './caisse.model';

@model()
export class Devises extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom: string;

  @property({
    type: 'string',
    required: true,
  })
  logo: string;

  @property({
    type: 'string',
  })
  currency?: string;

  @property({
    type: 'string',
  })
  symbole?: string;

  @property({
    type: 'number',
    required: true,
  })
  montant: number;

  @property({
    type: 'number',
    required: true,
  })
  contreVal: number;

  @belongsTo(() => Caisse)
  caisseId: string;

  constructor(data?: Partial<Devises>) {
    super(data);
  }
}

export interface DevisesRelations {
  // describe navigational properties here
}

export type DevisesWithRelations = Devises & DevisesRelations;
