// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {belongsTo, Entity, hasOne, model, property} from '@loopback/repository';
import {Bureau} from './bureau.model';
import {Caisse} from './caisse.model';
import {UserCredentials} from './user-credentials.model';

@model({
  settings: {
    strict: false,
  },
})
export class User extends Entity {
  // must keep it
  // add id:string<UUID>
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
    index: {
      unique: true,
    },
  })
  code?: string;

  @property({
    type: 'string',
  })
  nom?: string;

  @property({
    type: 'string',
  })
  prenom?: string;


  @property({
    type: 'string',
    jsonSchema: {
      minLength: 8,
      maxLength: 8,
    }
  })
  cin?: string;

  // must keep it
  @property({
    type: 'string',
  })
  username?: string;


  // must keep it

  // feat email unique
  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  email: string;

  @belongsTo(() => Bureau)
  bureauId: string;

  @hasOne(() => Caisse)
  caisse: Caisse;
  @property({
    type: 'boolean',
  })
  emailVerified?: boolean;

  @property({
    type: 'string',
    default: ' '
  })
  verificationToken?: string;

  @property({
    type: 'string',
  })
  creatdate?: string;


  @property({
    type: 'number',

  })
  firstConnexion?: Number;

  @property({
    type: 'boolean',
  })
  IsDeleted?: boolean;

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  roles: string[];

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
