import {Entity, hasMany, model, property} from '@loopback/repository';
import {User} from './user.model';
import {Journee} from './journee.model';
import {CoursDeChange} from './cours-de-change.model';

@model()
export class Bureau extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  idBCT: string;

  @property({
    type: 'string',
    required: true,
  })
  nom: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  adresse: string;

  @property({
    type: 'string',
    required: true,
  })
  telephone: string;

  @property({
    type: 'string',
  })
  logo?: string;

  @hasMany(() => User)
  users: User[];

  @hasMany(() => Journee)
  journees: Journee[];

  @hasMany(() => CoursDeChange)
  coursDeChanges: CoursDeChange[];

  constructor(data?: Partial<Bureau>) {
    super(data);
  }
}

export interface BureauRelations {
  // describe navigational properties here
}

export type BureauWithRelations = Bureau & BureauRelations;
