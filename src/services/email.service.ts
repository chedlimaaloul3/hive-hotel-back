import {bind, BindingScope} from '@loopback/core';
import {createTransport} from 'nodemailer';
import {EmailTemplate, NodeMailer, User} from '../models';

@bind({scope: BindingScope.TRANSIENT})
export class EmailService {
  /**
   * If using gmail see https://nodemailer.com/usage/using-gmail/
   */
  private static async setupTransporter() {
    return createTransport({
      host: process.env.SMTP_SERVER,
      port: +process.env.SMTP_PORT!,
      secure: false, // upgrade later with STARTTLS
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      },
    });
  }
  async sendResetPasswordMail(user: User, url: string): Promise<NodeMailer> {
    const transporter = await EmailService.setupTransporter();
    const emailTemplate = new EmailTemplate({
      to: user.email,
      subject: '[LB4] Reset Password Request',
      html: `
      <div>
          <p>Hi there,</p>
          <p style="color: red;">We received a request to reset the password for your account</p>
          <p>To reset your password click on the link provided below</p>
          <a href="${url}${user.resetKey}">Reset your password link</a>
          <p>If you didn’t request to reset your password, please ignore this email or reset your password to protect your account.</p>
          <p>Thanks</p>
          <p>LB4 team</p>
      </div>
      `,
    });
    return transporter.sendMail(emailTemplate);
  }

  async sendCreationUser(user: User, pwd: string, url: string): Promise<NodeMailer> {
    const transporter = await EmailService.setupTransporter();
    const emailTemplate = new EmailTemplate({
      to: user.email,
      subject: 'Bienvenue chez Hive Hôtel',
      html: `
      <div>
          <p>Bonjour,</p>
          <p style="color: red;">Bienvenue dans notre plateforme  Hive Hôtel</p>
          <p>Vos informations de connexion sont les suivantes:</p>
          <span><strong>Email: ${user.email}</strong></span>
          <br>
          <span><strong>Mot de passe: ${pwd}</strong></span>
          <p>pour vous connecter à notre plateforme cliquez sur le lien ci-dessous</p>
          <a href="${url}">Hive Hôtel</a>
          <p>Meilleures salutations,</p>
          <p>L'équipe Insight Plus</p>
      </div>
      `,
    });
    return transporter.sendMail(emailTemplate);
  }
}
