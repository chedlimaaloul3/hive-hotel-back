import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Bureau, BureauRelations, User, Journee, CoursDeChange} from '../models';
import {UserRepository} from './user.repository';
import {JourneeRepository} from './journee.repository';
import {CoursDeChangeRepository} from './cours-de-change.repository';

export class BureauRepository extends DefaultCrudRepository<
  Bureau,
  typeof Bureau.prototype.id,
  BureauRelations
> {

  public readonly users: HasManyRepositoryFactory<User, typeof Bureau.prototype.id>;

  public readonly journees: HasManyRepositoryFactory<Journee, typeof Bureau.prototype.id>;

  public readonly coursDeChanges: HasManyRepositoryFactory<CoursDeChange, typeof Bureau.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('JourneeRepository') protected journeeRepositoryGetter: Getter<JourneeRepository>, @repository.getter('CoursDeChangeRepository') protected coursDeChangeRepositoryGetter: Getter<CoursDeChangeRepository>,
  ) {
    super(Bureau, dataSource);
    this.coursDeChanges = this.createHasManyRepositoryFactoryFor('coursDeChanges', coursDeChangeRepositoryGetter,);
    this.registerInclusionResolver('coursDeChanges', this.coursDeChanges.inclusionResolver);
    this.journees = this.createHasManyRepositoryFactoryFor('journees', journeeRepositoryGetter,);
    this.registerInclusionResolver('journees', this.journees.inclusionResolver);
    this.users = this.createHasManyRepositoryFactoryFor('users', userRepositoryGetter,);
    this.registerInclusionResolver('users', this.users.inclusionResolver);
  }
}
