import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasOneRepositoryFactory, repository, BelongsToAccessor} from '@loopback/repository';
import {UserCredentialsRepository} from '.';
import {DbDataSource} from '../datasources';
import {User, UserCredentials, UserRelations, Bureau, Caisse} from '../models';
import {BureauRepository} from './bureau.repository';
import {CaisseRepository} from './caisse.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly userCredentials: HasOneRepositoryFactory<UserCredentials, typeof User.prototype.id>;

  public readonly bureau: BelongsToAccessor<Bureau, typeof User.prototype.id>;

  public readonly caisse: HasOneRepositoryFactory<Caisse, typeof User.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('UserCredentialsRepository') protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>, @repository.getter('BureauRepository') protected bureauRepositoryGetter: Getter<BureauRepository>, @repository.getter('CaisseRepository') protected caisseRepositoryGetter: Getter<CaisseRepository>,

  ) {
    super(User, dataSource);
    this.caisse = this.createHasOneRepositoryFactoryFor('caisse', caisseRepositoryGetter);
    this.registerInclusionResolver('caisse', this.caisse.inclusionResolver);
    this.bureau = this.createBelongsToAccessorFor('bureau', bureauRepositoryGetter,);
    this.registerInclusionResolver('bureau', this.bureau.inclusionResolver);
    this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialsRepositoryGetter);
    this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
  }
  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }
}
