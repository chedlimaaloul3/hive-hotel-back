import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Journee, JourneeRelations, Bureau} from '../models';
import {BureauRepository} from './bureau.repository';

export class JourneeRepository extends DefaultCrudRepository<
  Journee,
  typeof Journee.prototype.id,
  JourneeRelations
> {

  public readonly bureau: BelongsToAccessor<Bureau, typeof Journee.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('BureauRepository') protected bureauRepositoryGetter: Getter<BureauRepository>,
  ) {
    super(Journee, dataSource);
    this.bureau = this.createBelongsToAccessorFor('bureau', bureauRepositoryGetter,);
    this.registerInclusionResolver('bureau', this.bureau.inclusionResolver);
  }
}
