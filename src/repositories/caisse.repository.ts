import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Caisse, CaisseRelations, User, Devises} from '../models';
import {UserRepository} from './user.repository';
import {DevisesRepository} from './devises.repository';

export class CaisseRepository extends DefaultCrudRepository<
  Caisse,
  typeof Caisse.prototype.id,
  CaisseRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Caisse.prototype.id>;

  public readonly devises: HasManyRepositoryFactory<Devises, typeof Caisse.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('DevisesRepository') protected devisesRepositoryGetter: Getter<DevisesRepository>,
  ) {
    super(Caisse, dataSource);
    this.devises = this.createHasManyRepositoryFactoryFor('devises', devisesRepositoryGetter,);
    this.registerInclusionResolver('devises', this.devises.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }

  async generateCode() {

    let caisseCount = (await this.count()).count;
    let code = '';
    for (let i = 0; i < 3 - (caisseCount + 1).toString().length; i++) {
      code += '0'
    }
    code = 'CH' + '-' + code + (caisseCount + 1).toString();
    return code;

  }
}
