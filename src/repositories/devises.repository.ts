import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Devises, DevisesRelations, Caisse} from '../models';
import {CaisseRepository} from './caisse.repository';

export class DevisesRepository extends DefaultCrudRepository<
  Devises,
  typeof Devises.prototype.id,
  DevisesRelations
> {

  public readonly caisse: BelongsToAccessor<Caisse, typeof Devises.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('CaisseRepository') protected caisseRepositoryGetter: Getter<CaisseRepository>,
  ) {
    super(Devises, dataSource);
    this.caisse = this.createBelongsToAccessorFor('caisse', caisseRepositoryGetter,);
    this.registerInclusionResolver('caisse', this.caisse.inclusionResolver);
  }
}
