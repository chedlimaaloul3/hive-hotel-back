import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {CoursDeChange, CoursDeChangeRelations, Bureau} from '../models';
import {BureauRepository} from './bureau.repository';

export class CoursDeChangeRepository extends DefaultCrudRepository<
  CoursDeChange,
  typeof CoursDeChange.prototype.id,
  CoursDeChangeRelations
> {

  public readonly bureau: BelongsToAccessor<Bureau, typeof CoursDeChange.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('BureauRepository') protected bureauRepositoryGetter: Getter<BureauRepository>,
  ) {
    super(CoursDeChange, dataSource);
    this.bureau = this.createBelongsToAccessorFor('bureau', bureauRepositoryGetter,);
    this.registerInclusionResolver('bureau', this.bureau.inclusionResolver);
  }
}
