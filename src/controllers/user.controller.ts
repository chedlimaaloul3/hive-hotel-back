// Uncomment these imports to begin using these cool features!
import {authenticate, TokenService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {model, property, repository} from '@loopback/repository';
import {get, getModelSchemaRef, HttpErrors, param, post, put, requestBody, response, SchemaObject} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {genSalt, hash} from 'bcryptjs';
import generator from 'generate-password';
import _ from 'lodash';
import moment from 'moment-timezone';
import {v4 as uuidv4} from 'uuid';
import {TokenServiceBindings, UserServiceBindings} from '../keys';
import {Caisse, KeyAndPassword, NodeMailer, ResetPasswordInit, User} from '../models';
import {BureauRepository, CaisseRepository, UserRepository} from '../repositories';
import {Credentials, EmailService, MyUserService} from '../services';


// import {inject} from '@loopback/core';


@model()
export class Admin extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;



}
const CredentialsSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};
const url_login = "http://localhost:4300/account/login/"
const url = "http://localhost:4300/account/reset-password/"
export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(BureauRepository) protected bureaRepository: BureauRepository,
    @repository(CaisseRepository) protected caisseRepository: CaisseRepository,

    // email service injected here
    @inject('services.EmailService')
    public emailService: EmailService,
  ) { }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{token: string}> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    return {token};
  }

  @post('/users/verifCredentials', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async verifCredentials(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<User> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    ;


    return user;
  }

  @authenticate('jwt')
  @get('/whoAmI', {
    responses: {
      '200': {
        description: 'Return current user',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async whoAmI(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<any> {
    return {id: currentUserProfile[securityId]};
  }
  @get('/checkToken/{token}', {
    responses: {
      '200': {
        description: 'check token an return  user profil',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async CheckToken(
    @param.path.string('token') token: string

  ): Promise<UserProfile> {
    return this.jwtService.verifyToken(token);
  }

  @get('/users/{userId}', {
    responses: {
      '200': {
        description: 'Return current user',
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
      },
    },
  })
  async getuserById(
    @param.path.string('userId') id: string,

  ): Promise<User> {
    return this.userRepository.findById(id, {include: [{relation: 'bureau'}, {relation: 'caisse'}]});
  }

  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Admin, {
            title: 'NewUserAdmin',
          }),
        },
      },
    })
    AdminUserRequest: Admin,
  ): Promise<User> {

    AdminUserRequest.IsDeleted = false;
    AdminUserRequest.creatdate = moment().tz('Africa/Tunis').format('MM/DD/YYYY HH:mm:ss')
    const password = await hash(AdminUserRequest.password, await genSalt());
    const savedUser = await this.userRepository.create(
      _.omit(AdminUserRequest, 'password'),
    );

    await this.userRepository.userCredentials(savedUser.id).create({password});

    return savedUser;
  }

  @post('/signupUser', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async AddUser(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
          }),
        },
      },
    })
    UserRequest: User,
  ): Promise<any> {
    var pwd = generator.generate({
      length: 10,
      numbers: true
    });
    UserRequest.password = pwd;
    UserRequest.firstConnexion = 1;
    UserRequest.IsDeleted = false;
    UserRequest.creatdate = moment().tz('Africa/Tunis').format('MM/DD/YYYY HH:mm:ss')


    UserRequest.code = await this.generateCode(UserRequest.bureauId);
    const password = await hash(UserRequest.password, await genSalt());
    const savedUser = await this.userRepository.create(
      _.omit(UserRequest, 'password'),
    );

    await this.userRepository.userCredentials(savedUser.id).create({password});
    const caisse = new Caisse({code: await this.caisseRepository.generateCode(), etat: 'fermé', date: moment().tz('Africa/Tunis').format('MM/DD/YYYY HH:mm:ss')})
    const savedCaisse = await this.userRepository.caisse(savedUser.id).create(caisse);
    const nodeMailer: NodeMailer = await this.emailService.sendCreationUser(
      savedUser, UserRequest.password, url_login
    );

    return {user: savedUser, caisse: savedCaisse};
  }

  @put('/users/{id}')
  @response(204, {
    description: 'User PUT success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
        },
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,

    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            exclude: ['realm', 'emailVerified', 'verificationToken', 'username'],
          }),
        },
      },
    }) user: User,
  ): Promise<{res: boolean}> {


    try {
      await this.userRepository.replaceById(id, user);
      return {res: true}

    }
    catch (ex) {
      throw new HttpErrors.BadRequest(
        'erreur de mise à jour',
      );

    }






  }

  // We will add our password reset here

  @post('/reset-password/init', {
    responses: {
      '200': {
        description: 'msg',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })



  async resetPasswordInit(
    @requestBody() resetPasswordInit: ResetPasswordInit,
  ): Promise<{msg: string}> {
    // checks whether email is valid as per regex pattern provided
    const email = await this.validateEmail(resetPasswordInit.email);

    // At this point we are dealing with valid email.
    // Lets check whether there is an associated account
    const foundUser = await this.userRepository.findOne({
      where: {email, IsDeleted: false},
    });

    // No account found
    if (!foundUser) {
      throw new HttpErrors.NotFound(
        'No account associated with the provided email address.',
      );
    }

    // We generate unique reset key to associate with reset request
    foundUser.resetKey = uuidv4();

    try {
      // Updates the user to store their reset key with error handling
      await this.userRepository.updateById(foundUser.id, foundUser);
    } catch (e) {
      return e;
    }
    // Send an email to the user's email address
    const nodeMailer: NodeMailer = await this.emailService.sendResetPasswordMail(
      foundUser, url
    );

    // Nodemailer has accepted the request. All good
    if (nodeMailer.accepted.length) {
      const msg = 'An email with password reset instructions has been sent to the provided email'
      return {msg};
    }

    // Nodemailer did not complete the request alert the user
    throw new HttpErrors.InternalServerError(
      'Error sending reset password email',
    );
  }

  @post('/reset-password-first-login/init', {
    responses: {
      '200': {
        description: 'key',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })



  async resetPasswordInitFirstLogin(
    @requestBody() resetPasswordInit: ResetPasswordInit,
  ): Promise<{key: string}> {
    // checks whether email is valid as per regex pattern provided
    const email = await this.validateEmail(resetPasswordInit.email);

    // At this point we are dealing with valid email.
    // Lets check whether there is an associated account
    const foundUser = await this.userRepository.findOne({
      where: {email},
    });

    // No account found
    if (!foundUser) {
      throw new HttpErrors.NotFound(
        'No account associated with the provided email address.',
      );
    }

    // We generate unique reset key to associate with reset request
    foundUser.resetKey = uuidv4();

    try {
      // Updates the user to store their reset key with error handling
      await this.userRepository.updateById(foundUser.id, foundUser);
    } catch (e) {
      return e;
    }
    // Send an email to the user's email address
    /*const nodeMailer: NodeMailer = await this.emailService.sendResetPasswordMail(
      foundUser,
    );*/

    // Nodemailer has accepted the request. All good
    const key = foundUser.resetKey
    return {key};


    // Nodemailer did not complete the request alert the user
    throw new HttpErrors.InternalServerError(
      'Error sending reset password email',
    );
  }

  @put('/reset-password/finish', {
    responses: {
      '200': {
        description: 'msg',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async resetPasswordFinish(
    @requestBody() keyAndPassword: KeyAndPassword,
  ): Promise<{msg: string}> {
    // Checks whether password and reset key meet minimum security requirements
    const {resetKey, password} = await this.validateKeyPassword(keyAndPassword);

    // Search for a user using reset key
    const foundUser = await this.userRepository.findOne({
      where: {resetKey: resetKey},
    });

    // No user account found
    if (!foundUser) {
      throw new HttpErrors.NotFound(
        'No associated account for the provided reset key',
      );
    }

    // Encrypt password to avoid storing it as plain text
    const passwordHash = await hash(password, await genSalt());

    try {
      // Update user password with the newly provided password
      await this.userRepository
        .userCredentials(foundUser.id)
        .patch({password: passwordHash});

      // Remove reset key from database its no longer valid
      foundUser.resetKey = '';

      // Update the user removing the reset key
      await this.userRepository.updateById(foundUser.id, foundUser);
    } catch (e) {
      return e;
    }
    const msg = 'Password reset request completed successfully'
    return {msg};
  }

  async validateKeyPassword(keyAndPassword: KeyAndPassword): Promise<KeyAndPassword> {
    if (!keyAndPassword.password) {
      throw new HttpErrors.UnprocessableEntity(
        'Password must be minimum of 8 characters',
      );
    }

    if (keyAndPassword.password !== keyAndPassword.confirmPassword) {
      throw new HttpErrors.UnprocessableEntity(
        'Password and confirmation password do not match',
      );
    }

    if (
      _.isEmpty(keyAndPassword.resetKey) ||
      keyAndPassword.resetKey.length === 0 ||
      keyAndPassword.resetKey.trim() === ''
    ) {
      throw new HttpErrors.UnprocessableEntity('Reset key is mandatory');
    }

    return keyAndPassword;
  }

  async validateEmail(email: string): Promise<string> {
    const emailRegPattern = /\S+@\S+\.\S+/;
    if (!emailRegPattern.test(email)) {
      throw new HttpErrors.UnprocessableEntity('Invalid email address');
    }
    return email;
  }

  /*
   Generate code user
   */

  async generateCode(id: string) {
    let idBCT = await (await this.bureaRepository.findById(id)).idBCT;

    let usernumber = (await this.userRepository.count()).count;
    let code = '';
    for (let i = 0; i < 3 - (usernumber + 1).toString().length; i++) {
      code += '0'
    }
    code = 'BC' + idBCT + '-' + code + (usernumber + 1).toString();
    console.log(code);
    return code;

  }
}
