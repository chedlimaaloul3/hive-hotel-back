import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Journee,
  Bureau,
} from '../models';
import {JourneeRepository} from '../repositories';

export class JourneeBureauController {
  constructor(
    @repository(JourneeRepository)
    public journeeRepository: JourneeRepository,
  ) { }

  @get('/journees/{id}/bureau', {
    responses: {
      '200': {
        description: 'Bureau belonging to Journee',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Bureau)},
          },
        },
      },
    },
  })
  async getBureau(
    @param.path.string('id') id: typeof Journee.prototype.id,
  ): Promise<Bureau> {
    return this.journeeRepository.bureau(id);
  }
}
