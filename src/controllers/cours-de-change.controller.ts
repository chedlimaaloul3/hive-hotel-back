import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, HttpErrors, param,


  patch, post,




  put,

  requestBody,
  response
} from '@loopback/rest';
import {CoursDeChange} from '../models';
import {CoursDeChangeRepository} from '../repositories';

export class CoursDeChangeController {
  constructor(
    @repository(CoursDeChangeRepository)
    public coursDeChangeRepository: CoursDeChangeRepository,
  ) { }

  @post('/cours-de-changes')
  @response(200, {
    description: 'CoursDeChange model instance',
    content: {'application/json': {schema: getModelSchemaRef(CoursDeChange)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CoursDeChange, {
            title: 'NewCoursDeChange',
            exclude: ['id'],
          }),
        },
      },
    })
    coursDeChange: Omit<CoursDeChange, 'id'>,
  ): Promise<CoursDeChange> {
    return this.coursDeChangeRepository.create(coursDeChange);
  }
  @post('/cours-de-changes/creatAll')
  @response(200, {
    description: 'CoursDeChange model instance',
    content: {'application/json': {schema: getModelSchemaRef(CoursDeChange)}},
  })
  async createAll(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: getModelSchemaRef(CoursDeChange, {
              title: 'NewCoursDeChange',
              exclude: ['id'],
            }),
          }
        },
      },
    })
    coursDeChange: [Omit<CoursDeChange, 'id'>],
  ): Promise<CoursDeChange[]> {
    return this.coursDeChangeRepository.createAll(coursDeChange);
  }
  @get('/cours-de-changes/count')
  @response(200, {
    description: 'CoursDeChange model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CoursDeChange) where?: Where<CoursDeChange>,
  ): Promise<Count> {
    return this.coursDeChangeRepository.count(where);
  }

  @get('/cours-de-changes')
  @response(200, {
    description: 'Array of CoursDeChange model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CoursDeChange, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CoursDeChange) filter?: Filter<CoursDeChange>,
  ): Promise<CoursDeChange[]> {
    return this.coursDeChangeRepository.find(filter);
  }

  @patch('/cours-de-changes')
  @response(200, {
    description: 'CoursDeChange PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CoursDeChange, {partial: true}),
        },
      },
    })
    coursDeChange: CoursDeChange,
    @param.where(CoursDeChange) where?: Where<CoursDeChange>,
  ): Promise<Count> {
    return this.coursDeChangeRepository.updateAll(coursDeChange, where);
  }

  @get('/cours-de-changes/{id}')
  @response(200, {
    description: 'CoursDeChange model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CoursDeChange, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(CoursDeChange, {exclude: 'where'}) filter?: FilterExcludingWhere<CoursDeChange>
  ): Promise<CoursDeChange> {
    return this.coursDeChangeRepository.findById(id, filter);
  }

  @patch('/cours-de-changes/{id}')
  @response(204, {
    description: 'CoursDeChange PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CoursDeChange, {partial: true}),
        },
      },
    })
    coursDeChange: CoursDeChange,
  ): Promise<void> {
    await this.coursDeChangeRepository.updateById(id, coursDeChange);
  }

  @put('/cours-de-changes/{id}')
  @response(204, {
    description: 'CoursDeChange PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() coursDeChange: CoursDeChange,
  ): Promise<void> {
    await this.coursDeChangeRepository.replaceById(id, coursDeChange);
  }

  @put('/cours-de-changes/update')
  @response(204, {
    description: 'CoursDeChanges update success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
        },
      },
    },
  })
  async update(
    @requestBody({
      content: {
        'application/json': {
          schema:
          {

            type: 'array',
            items: getModelSchemaRef(CoursDeChange, {
            }),
          },
        },
      },
    }) coursDeChange: CoursDeChange[],
  ): Promise<any> {
    for (let i = 0; i < coursDeChange.length; i++) {


      try {
        await this.coursDeChangeRepository.replaceById(coursDeChange[i].id, coursDeChange[i]);


      }
      catch (ex) {
        throw new HttpErrors.BadRequest(
          'mise à jour n\'a pas terminé',
        );
      }

    }





  }

  @del('/cours-de-changes/{id}')
  @response(204, {
    description: 'CoursDeChange DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.coursDeChangeRepository.deleteById(id);
  }
}
