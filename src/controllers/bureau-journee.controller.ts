import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest';
import {
  Bureau,
  Journee
} from '../models';
import {BureauRepository} from '../repositories';

export class BureauJourneeController {
  constructor(
    @repository(BureauRepository) protected bureauRepository: BureauRepository,
  ) { }

  @get('/bureaus/{id}/journees', {
    responses: {
      '200': {
        description: 'Array of Bureau has many Journee',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Journee)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Journee>,
  ): Promise<Journee[]> {
    return this.bureauRepository.journees(id).find({where: {date_ouverture: {neq: ''}}}, filter);
  }

  @post('/bureaus/{id}/journees', {
    responses: {
      '200': {
        description: 'Bureau model instance',
        content: {'application/json': {schema: getModelSchemaRef(Journee)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Bureau.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Journee, {
            title: 'NewJourneeInBureau',
            exclude: ['id'],
            optional: ['bureauId']
          }),
        },
      },
    }) journee: Omit<Journee, 'id'>,
  ): Promise<Journee> {
    return this.bureauRepository.journees(id).create(journee);
  }

  @patch('/bureaus/{id}/journees', {
    responses: {
      '200': {
        description: 'Bureau.Journee PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Journee, {partial: true}),
        },
      },
    })
    journee: Partial<Journee>,
    @param.query.object('where', getWhereSchemaFor(Journee)) where?: Where<Journee>,
  ): Promise<Count> {
    return this.bureauRepository.journees(id).patch(journee, where);
  }

  @del('/bureaus/{id}/journees', {
    responses: {
      '200': {
        description: 'Bureau.Journee DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Journee)) where?: Where<Journee>,
  ): Promise<Count> {
    return this.bureauRepository.journees(id).delete(where);
  }
}
