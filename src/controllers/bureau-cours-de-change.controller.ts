import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Bureau,
  CoursDeChange,
} from '../models';
import {BureauRepository} from '../repositories';

export class BureauCoursDeChangeController {
  constructor(
    @repository(BureauRepository) protected bureauRepository: BureauRepository,
  ) { }

  @get('/bureaus/{id}/cours-de-changes', {
    responses: {
      '200': {
        description: 'Array of Bureau has many CoursDeChange',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(CoursDeChange)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<CoursDeChange>,
  ): Promise<CoursDeChange[]> {
    return this.bureauRepository.coursDeChanges(id).find(filter);
  }

  @post('/bureaus/{id}/cours-de-changes', {
    responses: {
      '200': {
        description: 'Bureau model instance',
        content: {'application/json': {schema: getModelSchemaRef(CoursDeChange)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Bureau.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CoursDeChange, {
            title: 'NewCoursDeChangeInBureau',
            exclude: ['id'],
            optional: ['bureauId']
          }),
        },
      },
    }) coursDeChange: Omit<CoursDeChange, 'id'>,
  ): Promise<CoursDeChange> {
    return this.bureauRepository.coursDeChanges(id).create(coursDeChange);
  }

  @patch('/bureaus/{id}/cours-de-changes', {
    responses: {
      '200': {
        description: 'Bureau.CoursDeChange PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CoursDeChange, {partial: true}),
        },
      },
    })
    coursDeChange: Partial<CoursDeChange>,
    @param.query.object('where', getWhereSchemaFor(CoursDeChange)) where?: Where<CoursDeChange>,
  ): Promise<Count> {
    return this.bureauRepository.coursDeChanges(id).patch(coursDeChange, where);
  }

  @del('/bureaus/{id}/cours-de-changes', {
    responses: {
      '200': {
        description: 'Bureau.CoursDeChange DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(CoursDeChange)) where?: Where<CoursDeChange>,
  ): Promise<Count> {
    return this.bureauRepository.coursDeChanges(id).delete(where);
  }
}
