import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Caisse,
  User,
} from '../models';
import {CaisseRepository} from '../repositories';

export class CaisseUserController {
  constructor(
    @repository(CaisseRepository)
    public caisseRepository: CaisseRepository,
  ) { }

  @get('/caisses/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Caisse',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Caisse.prototype.id,
  ): Promise<User> {
    return this.caisseRepository.user(id);
  }
}
