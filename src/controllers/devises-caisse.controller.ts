import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Devises,
  Caisse,
} from '../models';
import {DevisesRepository} from '../repositories';

export class DevisesCaisseController {
  constructor(
    @repository(DevisesRepository)
    public devisesRepository: DevisesRepository,
  ) { }

  @get('/devises/{id}/caisse', {
    responses: {
      '200': {
        description: 'Caisse belonging to Devises',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Caisse)},
          },
        },
      },
    },
  })
  async getCaisse(
    @param.path.string('id') id: typeof Devises.prototype.id,
  ): Promise<Caisse> {
    return this.devisesRepository.caisse(id);
  }
}
