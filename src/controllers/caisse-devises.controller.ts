import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Caisse,
  Devises,
} from '../models';
import {CaisseRepository} from '../repositories';

export class CaisseDevisesController {
  constructor(
    @repository(CaisseRepository) protected caisseRepository: CaisseRepository,
  ) { }

  @get('/caisses/{id}/devises', {
    responses: {
      '200': {
        description: 'Array of Caisse has many Devises',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Devises)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Devises>,
  ): Promise<Devises[]> {
    return this.caisseRepository.devises(id).find(filter);
  }

  @post('/caisses/{id}/devises', {
    responses: {
      '200': {
        description: 'Caisse model instance',
        content: {'application/json': {schema: getModelSchemaRef(Devises)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Caisse.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Devises, {
            title: 'NewDevisesInCaisse',
            exclude: ['id'],
            optional: ['caisseId']
          }),
        },
      },
    }) devises: Omit<Devises, 'id'>,
  ): Promise<Devises> {
    return this.caisseRepository.devises(id).create(devises);
  }

  @patch('/caisses/{id}/devises', {
    responses: {
      '200': {
        description: 'Caisse.Devises PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Devises, {partial: true}),
        },
      },
    })
    devises: Partial<Devises>,
    @param.query.object('where', getWhereSchemaFor(Devises)) where?: Where<Devises>,
  ): Promise<Count> {
    return this.caisseRepository.devises(id).patch(devises, where);
  }

  @del('/caisses/{id}/devises', {
    responses: {
      '200': {
        description: 'Caisse.Devises DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Devises)) where?: Where<Devises>,
  ): Promise<Count> {
    return this.caisseRepository.devises(id).delete(where);
  }
}
