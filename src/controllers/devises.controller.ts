import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody,
  response
} from '@loopback/rest';
import {Devises} from '../models';
import {DevisesRepository} from '../repositories';

export class DevisesController {
  constructor(
    @repository(DevisesRepository)
    public devisesRepository: DevisesRepository,
  ) { }

  @post('/devises')
  @response(200, {
    description: 'Devises model instance',
    content: {'application/json': {schema: getModelSchemaRef(Devises)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Devises, {
            title: 'NewDevises',
            exclude: ['id'],
          }),
        },
      },
    })
    devises: Omit<Devises, 'id'>,
  ): Promise<Devises> {
    return this.devisesRepository.create(devises);
  }

  @post('/devises/creatAll')
  @response(200, {
    description: 'Devises model instance',
    content: {'application/json': {schema: getModelSchemaRef(Devises)}},
  })
  async createAll(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: getModelSchemaRef(Devises, {
              title: 'NewDevises',
              exclude: ['id'],
            }),
          }
        },
      },
    })
    devises: [Omit<Devises, 'id'>],
  ): Promise<Devises[]> {
    console.log(devises[0])
    return this.devisesRepository.createAll(devises);
  }

  @get('/devises/count')
  @response(200, {
    description: 'Devises model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Devises) where?: Where<Devises>,
  ): Promise<Count> {
    return this.devisesRepository.count(where);
  }

  @get('/devises')
  @response(200, {
    description: 'Array of Devises model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Devises, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Devises) filter?: Filter<Devises>,
  ): Promise<Devises[]> {
    return this.devisesRepository.find(filter);
  }

  @patch('/devises')
  @response(200, {
    description: 'Devises PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Devises, {partial: true}),
        },
      },
    })
    devises: Devises,
    @param.where(Devises) where?: Where<Devises>,
  ): Promise<Count> {
    return this.devisesRepository.updateAll(devises, where);
  }

  @get('/devises/{id}')
  @response(200, {
    description: 'Devises model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Devises, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Devises, {exclude: 'where'}) filter?: FilterExcludingWhere<Devises>
  ): Promise<Devises> {
    return this.devisesRepository.findById(id, filter);
  }

  @patch('/devises/{id}')
  @response(204, {
    description: 'Devises PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Devises, {partial: true}),
        },
      },
    })
    devises: Devises,
  ): Promise<void> {
    await this.devisesRepository.updateById(id, devises);
  }

  @put('/devises/{id}')
  @response(204, {
    description: 'Devises PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() devises: Devises,
  ): Promise<void> {
    await this.devisesRepository.replaceById(id, devises);
  }

  @del('/devises/{id}')
  @response(204, {
    description: 'Devises DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.devisesRepository.deleteById(id);
  }
}
