import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  User,
  Caisse,
} from '../models';
import {UserRepository} from '../repositories';

export class UserCaisseController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @get('/users/{id}/caisse', {
    responses: {
      '200': {
        description: 'User has one Caisse',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Caisse),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Caisse>,
  ): Promise<Caisse> {
    return this.userRepository.caisse(id).get(filter);
  }

  @post('/users/{id}/caisse', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Caisse)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Caisse, {
            title: 'NewCaisseInUser',
            exclude: ['id'],
            optional: ['userId']
          }),
        },
      },
    }) caisse: Omit<Caisse, 'id'>,
  ): Promise<Caisse> {
    return this.userRepository.caisse(id).create(caisse);
  }

  @patch('/users/{id}/caisse', {
    responses: {
      '200': {
        description: 'User.Caisse PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Caisse, {partial: true}),
        },
      },
    })
    caisse: Partial<Caisse>,
    @param.query.object('where', getWhereSchemaFor(Caisse)) where?: Where<Caisse>,
  ): Promise<Count> {
    return this.userRepository.caisse(id).patch(caisse, where);
  }

  @del('/users/{id}/caisse', {
    responses: {
      '200': {
        description: 'User.Caisse DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Caisse)) where?: Where<Caisse>,
  ): Promise<Count> {
    return this.userRepository.caisse(id).delete(where);
  }
}
