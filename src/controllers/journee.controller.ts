import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, HttpErrors, param,


  patch, post,




  put,

  requestBody,
  response
} from '@loopback/rest';
import moment from 'moment-timezone';
import {Journee} from '../models';
import {JourneeRepository} from '../repositories';

export class JourneeController {
  constructor(
    @repository(JourneeRepository)
    public journeeRepository: JourneeRepository,
  ) { }

  @post('/journees')
  @response(200, {
    description: 'Journee model instance',
    content: {'application/json': {schema: getModelSchemaRef(Journee)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Journee, {
            title: 'NewJournee',
            exclude: ['id'],
          }),
        },
      },
    })
    journee: Omit<Journee, 'id'>,
  ): Promise<Journee> {
    journee.date_ouverture = moment().tz('Africa/Tunis').format('MM/DD/YYYY HH:mm:ss')
    journee.date_fermuture = '---'
    journee.code_user_ferm = '---'
    return this.journeeRepository.create(journee);
  }

  @get('/journees/count')
  @response(200, {
    description: 'Journee model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Journee) where?: Where<Journee>,
  ): Promise<Count> {
    return this.journeeRepository.count(where);
  }

  @get('/journees')
  @response(200, {
    description: 'Array of Journee model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Journee, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Journee) filter?: Filter<Journee>,
  ): Promise<Journee[]> {
    return this.journeeRepository.find(filter);
  }

  @patch('/journees')
  @response(200, {
    description: 'Journee PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Journee, {partial: true}),
        },
      },
    })
    journee: Journee,
    @param.where(Journee) where?: Where<Journee>,
  ): Promise<Count> {
    return this.journeeRepository.updateAll(journee, where);
  }

  @get('/journees/{id}')
  @response(200, {
    description: 'Journee model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Journee, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Journee, {exclude: 'where'}) filter?: FilterExcludingWhere<Journee>
  ): Promise<Journee> {
    return this.journeeRepository.findById(id, filter);
  }

  @patch('/journees/{id}')
  @response(204, {
    description: 'Journee PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Journee, {partial: true}),
        },
      },
    })
    journee: Journee,
  ): Promise<void> {
    await this.journeeRepository.updateById(id, journee);
  }

  @put('/journees/{id}')
  @response(204, {
    description: 'Journee PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() journee: Journee,
  ): Promise<void> {
    try {
      await this.journeeRepository.replaceById(id, journee);
    }
    catch (ex) {
      throw new HttpErrors.NotAcceptable(
        'erreur de mise à jour',
      );

    }
  }

  @del('/journees/{id}')
  @response(204, {
    description: 'Journee DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.journeeRepository.deleteById(id);
  }
}
