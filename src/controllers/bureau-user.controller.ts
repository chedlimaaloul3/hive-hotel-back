import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest';
import {
  Bureau,
  User
} from '../models';
import {BureauRepository} from '../repositories';

export class BureauUserController {
  constructor(
    @repository(BureauRepository) protected bureauRepository: BureauRepository,
  ) { }

  @get('/bureaus/{id}/users', {
    responses: {
      '200': {
        description: 'Array of Bureau has many User',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<User>,
  ): Promise<User[]> {
    return this.bureauRepository.users(id).find({include: [{relation: 'caisse'}], where: {IsDeleted: false}});
  }

  @post('/bureaus/{id}/users', {
    responses: {
      '200': {
        description: 'Bureau model instance',
        content: {'application/json': {schema: getModelSchemaRef(User)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Bureau.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUserInBureau',
            exclude: ['id'],
            optional: ['bureauId']
          }),
        },
      },
    }) user: Omit<User, 'id'>,
  ): Promise<User> {
    return this.bureauRepository.users(id).create(user);
  }

  @patch('/bureaus/{id}/users', {
    responses: {
      '200': {
        description: 'Bureau.User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: Partial<User>,
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>,
  ): Promise<Count> {
    return this.bureauRepository.users(id).patch(user, where);
  }

  @del('/bureaus/{id}/users', {
    responses: {
      '200': {
        description: 'Bureau.User DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>,
  ): Promise<Count> {
    return this.bureauRepository.users(id).delete(where);
  }
}
