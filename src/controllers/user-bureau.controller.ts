import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  User,
  Bureau,
} from '../models';
import {UserRepository} from '../repositories';

export class UserBureauController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @get('/users/{id}/bureau', {
    responses: {
      '200': {
        description: 'Bureau belonging to User',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Bureau)},
          },
        },
      },
    },
  })
  async getBureau(
    @param.path.string('id') id: typeof User.prototype.id,
  ): Promise<Bureau> {
    return this.userRepository.bureau(id);
  }
}
