import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  CoursDeChange,
  Bureau,
} from '../models';
import {CoursDeChangeRepository} from '../repositories';

export class CoursDeChangeBureauController {
  constructor(
    @repository(CoursDeChangeRepository)
    public coursDeChangeRepository: CoursDeChangeRepository,
  ) { }

  @get('/cours-de-changes/{id}/bureau', {
    responses: {
      '200': {
        description: 'Bureau belonging to CoursDeChange',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Bureau)},
          },
        },
      },
    },
  })
  async getBureau(
    @param.path.string('id') id: typeof CoursDeChange.prototype.id,
  ): Promise<Bureau> {
    return this.coursDeChangeRepository.bureau(id);
  }
}
